## FastSpeech 2 - Pytorch Implementation (Updated)

## Guide:
Install dependencies:

```
pip3 install -r requirements.txt
pip3 install --pre torch==1.6.0.dev20200428 -f https://download.pytorch.org/whl/nightly/cu102/torch_nightly.html
```

## Data preparation:

Data should be in LJSpeech format (to ensure things work smoothly).
```
fastspeechdata
        ├── metadata.csv
        ├── wavs
        │   ├── LJ001-0001.wav
        │   ├── LJ001-0002.wav
        │   ├── LJ001-0003.wav
```

metadata.csv is  tab-seperated file : <name>|<None>|<text>

## Running MFA:

(Assuming data is in `../fastspeechdata`):

```
python3 prepare_align.py

wget https://github.com/MontrealCorpusTools/Montreal-Forced-Aligner/releases/download/v1.1.0-beta.2/montreal-forced-aligner_linux.tar.gz
tar -zxvf montreal-forced-aligner_linux.tar.gz

./montreal-forced-aligner/bin/mfa_train_and_align  ../fastspeechdata/wavs lexicon.txt preprocessed/LJSpeech/TextGrid

python3 preprocess.py
```

After preprocessing, you will get a stat.txt file in your hp.preprocessed_path/, recording the maximum and minimum values of the fundamental frequency and energy values throughout the entire corpus. You have to modify the f0 and energy parameters in the hparams.py according to the content of stat.txt

## Train:

`python3 train.py`


Detailed Readme [here](https://github.com/ming024/FastSpeech2#fastspeech-2---pytorch-implementation)